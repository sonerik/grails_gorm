package grails_gorm

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Comment)
@Mock([User])
class CommentSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "creating comment without user should produce error"() {
        setup:
        def comment = new Comment(text: "text")

        when:
        comment.save()

        then:
        comment.hasErrors()

        cleanup:
        comment.delete()
    }

    void "creating comment with user should be fine"() {
        setup:
        def u = new User(name: "Alex")
        def comment = new Comment(text: "text")

        when:
        u.addToComments(comment)
        u.save()

        then:
        !u.hasErrors()
        u.getComments().size() == 1

        cleanup:
        u.delete()
    }

}
