package grails_gorm

import grails.test.mixin.TestFor
import spock.lang.Specification
/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(User)
class UserSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "list all users"() {
        setup:
        10.times {
            new User(name: "x").save()
        }

        expect:
        User.list().size() == 10

        cleanup:
        User.deleteAll(User.list())
    }

    void "deleteAll should delete all items"() {
        setup:
        10.times {
            new User(name: "x").save()
        }

        when:
        User.deleteAll(User.list())

        then:
        User.list().size() == 0
    }

}
