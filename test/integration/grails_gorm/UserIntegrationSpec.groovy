package grails_gorm

import grails.test.spock.IntegrationSpec

class UserIntegrationSpec extends IntegrationSpec {

    void "delete user should also delete comments related to this user"() {
        setup:
        def u = new User(name: "x")
        def c = new Comment(text: "text")
        u.addToComments(c)
        u.save(flush: true)

        expect:
        !u.hasErrors()

        when:
        u.delete(flush: true)

        then:
        Comment.count == 0

    }

}
