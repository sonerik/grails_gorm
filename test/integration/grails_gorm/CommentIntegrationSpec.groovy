package grails_gorm
import grails.test.spock.IntegrationSpec

class CommentIntegrationSpec extends IntegrationSpec {

    void "querying comments by user"() {
        setup:
        def u1 = new User(name: "Alex")
        def u2 = new User(name: "Valentine")

        when:
        7.times {
            u1.addToComments(new Comment(text: "u1"))
        }

        u1.save()

        15.times {
            u2.addToComments(new Comment(text: "u2"))
        }

        u2.save()

        then:
        Comment.list().size() == 7 + 15

        Comment.findAllByUser(u1).size() == 7
        Comment.findAllByUser(u2).size() == 15

        cleanup:
        u1.delete()
        u2.delete()
    }

}