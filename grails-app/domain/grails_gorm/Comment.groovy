package grails_gorm

class Comment {

    static belongsTo = [user: User]

    static constraints = {
    }

    String text
}
